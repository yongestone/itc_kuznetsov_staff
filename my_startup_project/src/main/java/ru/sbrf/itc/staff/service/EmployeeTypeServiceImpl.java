package ru.sbrf.itc.staff.service;


import com.google.gson.Gson;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sbrf.itc.staff.entity.EmployeeType;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

@Service
public class EmployeeTypeServiceImpl implements EmployeeTypeService {
    private SessionFactory sessionFactory;

    public EmployeeTypeServiceImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void add(EmployeeType employeeType) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(employeeType);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(EmployeeType employeeType) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.delete(employeeType);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(EmployeeType employeeType) throws InvocationTargetException, IllegalAccessException {
        try (Session session = getSessionFactory().openSession()) {
            EmployeeType employeeType1 = findEmployeeType(employeeType.getId());
            session.beginTransaction();
            BeanUtilsBean notNull=new NullAwareBeanUtilsBean();
            notNull.copyProperties(employeeType1, employeeType);
            session.update(employeeType1);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<EmployeeType> findAll() {
        try (Session session = getSessionFactory().openSession()) {
            List<EmployeeType> showList = session.createQuery("from EmployeeType").list();
            return showList;
        }
    }

    @Override
    public EmployeeType findEmployeeType(Long employeeTypeId) {
        try (Session session = getSessionFactory().openSession()) {
            EmployeeType employeeType = session.get(EmployeeType.class, employeeTypeId);
            return employeeType;
        }
    }

    @Override
    public void saveEmployeeType(List<EmployeeType> employeeTypeList) {
        Gson gson = new Gson();
        try {
            FileWriter writer = new FileWriter(InitHelper.TYPE_FILE_NAME, false);
            gson.toJson(employeeTypeList, writer);
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
