package ru.sbrf.itc.staff.service;

import com.google.gson.Gson;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sbrf.itc.staff.entity.Employee;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private SessionFactory sessionFactory;

    public EmployeeServiceImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void add(Employee employee) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(employee);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Employee employee) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.delete(employee);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Employee> findAll() {
        try (Session session = getSessionFactory().openSession()) {
            List<Employee> showList = session.createQuery("from Employee").list();
            return showList;
        }
    }

    @Override
    public List<Employee> findParent() {
        try (Session session = getSessionFactory().openSession()) {
            List<Employee> showList = session.createQuery("from Employee where employeeType.head='true'").list();
            return showList;
        }
    }

    @Override
    public Employee findEmployee(Long employeeId) {
        try (Session session = getSessionFactory().openSession()) {
            Employee employee = session.get(Employee.class, employeeId);
            return employee;
        }
    }

    @Override
    public void update(Employee employee) throws InvocationTargetException, IllegalAccessException {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            Employee employee1 = findEmployee(employee.getId());
            BeanUtilsBean notNull=new NullAwareBeanUtilsBean();
            notNull.copyProperties(employee1, employee);
            session.update(employee1);
            session.getTransaction().commit();
        }
    }

    @Override
    public void saveEmployee(List<Employee> employeeList) {
        Gson gson = new Gson();
        try {
            FileWriter writer = new FileWriter(InitHelper.EMPLOYEE_FILE_NAME, false);
            gson.toJson(employeeList, writer);
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
