package ru.sbrf.itc.staff.service;

public interface Command {
    String name();

    String description();

    void execute(String[] args);
}
