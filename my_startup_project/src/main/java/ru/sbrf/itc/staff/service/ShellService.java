package ru.sbrf.itc.staff.service;


import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class ShellService {
    private InputStream inputStream = System.in;
    private OutputStream outputStream = System.out;

    public ShellService(InputStream inputStream, OutputStream outputStream) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    private boolean terminate;
    private Map<String, Command> commands = new HashMap<String, Command>();

    public void setCommands(List<Command> commandsList) {
        for (Command command : commandsList) {
            addCommand(command);
        }
    }

    class QuitCommand implements Command {

        public String name() {
            return "quit";
        }

        public String description() {
            return "";
        }

        public void execute(String[] args) {
            terminate = true;
        }
    }

    class HelpCommand implements Command {

        public String name() {
            return "help";
        }

        public String description() {
            return "";
        }

        public void execute(String[] args) {
            commands.forEach((k, v) -> {
                try {
                    outputStream.write(("\"" + v.name() + "\"" + " - " + v.description() + "\n").getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public ShellService() {
        addCommand(new QuitCommand());
        addCommand(new HelpCommand());
    }

    public void addCommand(Command command) {
        commands.put(command.name(), command);
    }

    public void run() throws IOException {
        outputStream.write("\n".getBytes());
        outputStream.write("You may use \"help\": \n".getBytes());
        while (!terminate) {
            outputStream.write("> ".getBytes());
            Scanner scanner = new Scanner(inputStream);
            String cmdLine = scanner.nextLine();
            String[] args = cmdLine.split(" ");
            if (args.length > 0) {
                if (commands.containsKey(args[0])) {
                    Command command = commands.get(args[0]);
                    command.execute(args);
                }
            }
        }
    }
}