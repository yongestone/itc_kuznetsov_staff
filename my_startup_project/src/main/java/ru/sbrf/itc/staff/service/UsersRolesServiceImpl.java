package ru.sbrf.itc.staff.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sbrf.itc.staff.entity.UserRoles;

import java.util.List;

@Service
public class UsersRolesServiceImpl implements UsersRolesService {
    private SessionFactory sessionFactory;

    public UsersRolesServiceImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void add(UserRoles user) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public UserRoles findEmployee(String name) {
        try (Session session = getSessionFactory().openSession()) {
            Query query = session.createQuery("from UserRoles where username=:name");
            query.setParameter("name", name);
            List<UserRoles> userRoles = query.list();
            UserRoles user = userRoles.get(0);
            return user;
        }
    }

    @Override
    public List<UserRoles> findAll(String name) {
        try (Session session = getSessionFactory().openSession()) {
            Query query = session.createQuery("from UserRoles where username=:name");
            query.setParameter("name", name);
            List<UserRoles> showList = query.list();
            return showList;
        }
    }
}
