package ru.sbrf.itc.staff.service;

import ru.sbrf.itc.staff.entity.Employee;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface EmployeeService {
    void add(Employee employee);

    void delete(Employee employee);

    void update(Employee employee) throws InvocationTargetException, IllegalAccessException;

    List<Employee> findAll();

    List<Employee> findParent();

    Employee findEmployee(Long employeeId);

    void saveEmployee(List<Employee> employeeList);
}
