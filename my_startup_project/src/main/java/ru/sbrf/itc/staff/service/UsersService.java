package ru.sbrf.itc.staff.service;

import ru.sbrf.itc.staff.entity.Users;


public interface UsersService {
    void add(Users user);

    Users findEmployee(String name);
}
