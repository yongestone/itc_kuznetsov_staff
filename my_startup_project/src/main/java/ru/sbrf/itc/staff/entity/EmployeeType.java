package ru.sbrf.itc.staff.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "etype")
@org.hibernate.annotations.Entity(dynamicUpdate = true, selectBeforeUpdate = true)
public class EmployeeType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Column(name = "rate", precision = 20, scale = 10)
    private BigDecimal rate;
    @Column(name = "ratemax", precision = 20, scale = 10)
    private BigDecimal ratemax;
    @Column(name = "ratehead", precision = 20, scale = 10)
    private BigDecimal ratehead;
    private boolean head;
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "typeid")
    private Set<Employee> employees;

    public EmployeeType(Long id, String name, BigDecimal rate, BigDecimal ratemax, BigDecimal ratehead, Boolean head) {
        this.id = id;
        this.name = name;
        this.rate = rate;
        this.ratemax = ratemax;
        this.ratehead = ratehead;
        this.head = head;
    }

    public EmployeeType() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHead() {
        return head;
    }

    public void setHead(boolean head) {
        this.head = head;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getRatemax() {
        return ratemax;
    }

    public void setRatemax(BigDecimal ratemax) {
        this.ratemax = ratemax;
    }

    public BigDecimal getRatehead() {
        return ratehead;
    }

    public void setRatehead(BigDecimal ratehead) {
        this.ratehead = ratehead;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }
}
