package ru.sbrf.itc.staff.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sbrf.itc.staff.entity.Users;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {
    private SessionFactory sessionFactory;

    public UsersServiceImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void add(Users user) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public Users findEmployee(String name) {
        try (Session session = getSessionFactory().openSession()) {

            Query query = session.createQuery("from Users where name=:name");
            query.setParameter("name", name);
            List<Users> userRoles = query.list();
            Users user = userRoles.get(0);
            return user;
        }
    }
}
