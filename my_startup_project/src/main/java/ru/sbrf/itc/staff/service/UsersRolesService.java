package ru.sbrf.itc.staff.service;

import ru.sbrf.itc.staff.entity.UserRoles;

import java.util.List;


public interface UsersRolesService {
    void add(UserRoles user);

    UserRoles findEmployee(String name);

    List<UserRoles> findAll(String name);
}
