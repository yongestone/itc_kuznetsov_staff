package ru.sbrf.itc.staff.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.entity.EmployeeType;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

@Service
public class CalcServiceImpl implements CalcService {
    private static final String Sales = "Sales";
    private static final String Manager = "Manager";
    private EmployeeService employeeServiceImpl;

    @Override
    public Period getPeriodNow(Employee employee, Date date) {
        Period period = Period.between(employee.getReceiptDate().toLocalDate(), date.toLocalDate());
        return period;
    }

    @Override
    public List<Employee> allSubordinates(Employee employee) {
        List<Employee> SubordinatesList = new ArrayList<Employee>();

        List<Employee> employeeList = employee.getSubordinates();
        if (employeeList != null) {
            for (Employee subEmployee : employeeList) {
                SubordinatesList.add(subEmployee);
                List<Employee> employeeListDown = allSubordinates(subEmployee);
                SubordinatesList.addAll(employeeListDown);
            }
        }
        return SubordinatesList;
    }

    @Override
    public BigDecimal getMulti(Employee employee, Date date) {
        EmployeeType employeeType = employee.getEmployeeType();
        BigDecimal rate = employeeType.getRate();
        BigDecimal ratemax = employeeType.getRatemax();
        BigDecimal yearRate = rate.multiply(BigDecimal.valueOf(getPeriodNow(employee, date).getYears()));
        BigDecimal maxMulti = yearRate.min(ratemax);
        BigDecimal multi = maxMulti.add(BigDecimal.ONE);
        return multi;
    }

    @Override
    public BigDecimal calculateSalesSalary(Employee employee, Date date) {
        BigDecimal subordinatesSalary = BigDecimal.ZERO;
        List<Employee> employeeList = allSubordinates(employee);
        if (employeeList != null) {
            for (Employee sub : employeeList) {
                subordinatesSalary = subordinatesSalary.add(calculateSalary(sub, date));
            }
        }
        return subordinatesSalary;
    }

    @Override
    public BigDecimal calculateManagerSalary(Employee employee, Date date) {
        BigDecimal subordinatesSalary = BigDecimal.ZERO;
        List<Employee> employeeList = employee.getSubordinates();
        if (employeeList != null) {
            for (Employee sub : employeeList) {
                subordinatesSalary = subordinatesSalary.add(calculateSalary(sub, date));
            }
        }
        return subordinatesSalary;
    }

    @Override
    public BigDecimal calculateSubordinatesSalary(Employee employee, Date date) {
        BigDecimal subordinatesSalary = BigDecimal.ZERO;
        if (employee.getEmployeeType().getName().equals(Sales)) {
            subordinatesSalary = calculateSalesSalary(employee, date);
        } else if (employee.getEmployeeType().getName().equals(Manager)) {
            subordinatesSalary = calculateManagerSalary(employee, date);
        }
        subordinatesSalary = subordinatesSalary.multiply(employee.getEmployeeType().getRatehead());
        return subordinatesSalary;
    }

    @Override
    public BigDecimal calculateSalary(Employee employee, Date date) {
        BigDecimal finalSalary = calculateSubordinatesSalary(employee, date);
        finalSalary = finalSalary.add(employee.getSalary().multiply(getMulti(employee, date)));
        return finalSalary;
    }

    @Override
    public BigDecimal calculateAllSalary(Date date) {
        BigDecimal finalSalary = BigDecimal.ZERO;
        List<Employee> employeeList = employeeServiceImpl.findAll();
        if (employeeList != null) {
            for (Employee employee : employeeList) {
                finalSalary = finalSalary.add(calculateSalary(employee, date));
            }
        }
        return finalSalary;
    }

    @Autowired
    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeServiceImpl = employeeService;
    }

}
