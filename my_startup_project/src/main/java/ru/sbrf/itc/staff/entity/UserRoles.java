package ru.sbrf.itc.staff.entity;

import javax.persistence.*;

@Entity
@Table(name = "roles")
@org.hibernate.annotations.Entity(dynamicUpdate = true, selectBeforeUpdate = true)
public class UserRoles {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "userId")
    private Long userId;
    private String username;
    private String role;

    public UserRoles(Long id, Long userId, String name, String role) {
        this.id = id;
        this.userId = userId;
        this.username = name;
        this.role = role;
    }

    public UserRoles() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
