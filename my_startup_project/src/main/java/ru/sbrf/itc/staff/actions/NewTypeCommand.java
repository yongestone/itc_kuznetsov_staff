package ru.sbrf.itc.staff.actions;

import org.springframework.beans.factory.annotation.Autowired;
import ru.sbrf.itc.staff.entity.EmployeeType;
import ru.sbrf.itc.staff.service.Command;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

import java.math.BigDecimal;
import java.util.Scanner;

public class NewTypeCommand implements Command {
    private EmployeeTypeService employeeTypeServiceImpl;

    public String name() {
        return "newtype";
    }

    public String description() {
        return "";
    }

    public void execute(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Name: ");
        String name = in.nextLine();
        System.out.print("Head: ");
        int a = in.nextInt();
        Boolean head;
        if (a == 0) {
            head = false;
        } else {
            head = true;
        }
        System.out.print("Rate: ");
        BigDecimal rate = in.nextBigDecimal();
        System.out.print("RateMax: ");
        BigDecimal ratemax = in.nextBigDecimal();
        System.out.print("RateHead: ");
        BigDecimal ratehead = in.nextBigDecimal();
        EmployeeType eType = new EmployeeType(0L, name, rate, ratemax, ratehead, head);
        employeeTypeServiceImpl.add(eType);
    }

    @Autowired
    public void setEmployeeTypeService(EmployeeTypeService employeeTypeServiceImpl) {
        this.employeeTypeServiceImpl = employeeTypeServiceImpl;
    }
}