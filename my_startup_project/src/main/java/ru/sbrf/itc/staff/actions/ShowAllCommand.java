package ru.sbrf.itc.staff.actions;

import org.springframework.beans.factory.annotation.Autowired;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.entity.EmployeeType;
import ru.sbrf.itc.staff.service.Command;
import ru.sbrf.itc.staff.service.EmployeeService;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

import java.text.MessageFormat;
import java.util.List;

public class ShowAllCommand implements Command {
    private EmployeeTypeService employeeTypeServiceImpl;
    private EmployeeService employeeServiceImpl;

    public String name() {
        return "showall";
    }

    public String description() {
        return "";
    }

    public void execute(String[] args) {
        System.out.print("> ");
        List<Employee> showAll = employeeServiceImpl.findAll();
        for (Employee employee : showAll) {
            String msg = MessageFormat.format("\tId: {0} , ФИО: {1}, Должность: {2}\n", employee.getId(), employee.getName(), employee.getEmployeeType().getName());
            System.out.print(msg);
        }
        System.out.println();
        List<EmployeeType> showType = employeeTypeServiceImpl.findAll();
        for (EmployeeType employeeType : showType) {
            String msg = MessageFormat.format("\t(Type) Id: {0} , Должность: {1}\n", employeeType.getId(), employeeType.getName());
            System.out.print(msg);
        }
    }

    @Autowired
    public void setEmployeeTypeService(EmployeeTypeService employeeTypeServiceImpl) {
        this.employeeTypeServiceImpl = employeeTypeServiceImpl;
    }

    @Autowired
    public void setEmployeeService(EmployeeService employeeServiceImpl) {
        this.employeeServiceImpl = employeeServiceImpl;
    }
}
