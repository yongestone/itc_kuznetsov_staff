package ru.sbrf.itc.staff.actions;

import org.springframework.beans.factory.annotation.Autowired;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.service.Command;
import ru.sbrf.itc.staff.service.EmployeeService;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Scanner;

public class NewUserCommand implements Command {
    private EmployeeTypeService employeeTypeServiceImpl;
    private EmployeeService employeeServiceImpl;

    public String name() {
        return "newuser";
    }

    public String description() {
        return "";
    }

    public void execute(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Name: ");
        String name = in.nextLine();
        System.out.print("typeID: ");
        Long typeId = in.nextLong();
        System.out.print("parentID: ");
        Long parentId = in.nextLong();
        System.out.print("Salary: ");
        BigDecimal salary = in.nextBigDecimal();
        System.out.print("Date in format(YYYY-MM-DD): ");
        in.nextLine();
        Date date = Date.valueOf(in.nextLine());
        if (parentId == 0) {
            parentId = null;
        }
        Employee employee = new Employee(0L, name, employeeTypeServiceImpl.findEmployeeType(typeId), parentId, salary, date);
        employeeServiceImpl.add(employee);
    }

    @Autowired
    public void setEmployeeTypeService(EmployeeTypeService employeeTypeServiceImpl) {
        this.employeeTypeServiceImpl = employeeTypeServiceImpl;
    }

    @Autowired
    public void setEmployeeService(EmployeeService employeeServiceImpl) {
        this.employeeServiceImpl = employeeServiceImpl;
    }
}