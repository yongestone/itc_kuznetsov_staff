package ru.sbrf.itc.staff;

import org.hibernate.SessionFactory;
import ru.sbrf.itc.staff.service.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class StaffApp {
    public static void main(String[] args) throws Exception {
        System.out.println("Startup...");
        Class.forName("org.h2.Driver").newInstance();
        System.out.println("Connection complete");
        //init
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        InitHelper helper = (InitHelper) context.getBean("initHelper");
//        helper.init();
        System.out.println("Init database complete");
        //shell
        ShellService shellService = (ShellService) context.getBean("shellService");
        shellService.run();

        closeSessionFactory(context);
    }

    private static void closeSessionFactory(ApplicationContext context) {
        SessionFactory sessionFactory = context.getBean(SessionFactory.class);
        sessionFactory.close();
        System.out.println("Connection close");
    }
}



