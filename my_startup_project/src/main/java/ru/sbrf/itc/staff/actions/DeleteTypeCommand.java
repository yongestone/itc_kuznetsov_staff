package ru.sbrf.itc.staff.actions;

import org.springframework.beans.factory.annotation.Autowired;
import ru.sbrf.itc.staff.entity.EmployeeType;
import ru.sbrf.itc.staff.service.Command;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

public class DeleteTypeCommand implements Command {
    private EmployeeTypeService employeeTypeServiceImpl;

    public String name() {
        return "deletetype";
    }

    public String description() {
        return "use \"deletetype {id}\"";
    }

    public void execute(String[] args) {
        EmployeeType employeeType = employeeTypeServiceImpl.findEmployeeType(Long.valueOf(args[args.length - 1]));
        employeeTypeServiceImpl.delete(employeeType);
    }

    @Autowired
    public void setEmployeeTypeService(EmployeeTypeService employeeTypeServiceImpl) {
        this.employeeTypeServiceImpl = employeeTypeServiceImpl;
    }
}