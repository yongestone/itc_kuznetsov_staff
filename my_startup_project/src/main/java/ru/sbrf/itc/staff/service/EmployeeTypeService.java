package ru.sbrf.itc.staff.service;

import ru.sbrf.itc.staff.entity.EmployeeType;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface EmployeeTypeService {
    void add(EmployeeType employeeType);

    void delete(EmployeeType employeeType);

    void update(EmployeeType employeeType) throws InvocationTargetException, IllegalAccessException;

    List<EmployeeType> findAll();

    EmployeeType findEmployeeType(Long employeeTypeId);

    void saveEmployeeType(List<EmployeeType> employeeTypeList);
}
