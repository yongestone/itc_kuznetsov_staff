package ru.sbrf.itc.staff.actions;

import org.springframework.beans.factory.annotation.Autowired;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.service.Command;
import ru.sbrf.itc.staff.service.EmployeeService;

public class DeleteCommand implements Command {
    private EmployeeService employeeServiceImpl;

    public String name() {
        return "delete";
    }

    public String description() {
        return "use \"delete {id}\"";
    }

    public void execute(String[] args) {
        Employee employee = employeeServiceImpl.findEmployee(Long.valueOf(args[args.length - 1]));
        employeeServiceImpl.delete(employee);
    }

    @Autowired
    public void setEmployeeService(EmployeeService employeeServiceImpl) {
        this.employeeServiceImpl = employeeServiceImpl;
    }
}