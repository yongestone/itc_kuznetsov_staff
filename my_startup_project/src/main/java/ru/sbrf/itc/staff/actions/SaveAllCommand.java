package ru.sbrf.itc.staff.actions;

import org.springframework.beans.factory.annotation.Autowired;
import ru.sbrf.itc.staff.service.Command;
import ru.sbrf.itc.staff.service.EmployeeService;
import ru.sbrf.itc.staff.service.EmployeeTypeService;


public class SaveAllCommand implements Command {
    private EmployeeTypeService employeeTypeServiceImpl;
    private EmployeeService employeeServiceImpl;

    public String name() {
        return "saveall";
    }

    public String description() {
        return "save tables to database";
    }

    public void execute(String[] args) {
        employeeServiceImpl.saveEmployee(employeeServiceImpl.findAll());
        employeeTypeServiceImpl.saveEmployeeType(employeeTypeServiceImpl.findAll());
    }

    @Autowired
    public void setEmployeeTypeService(EmployeeTypeService employeeTypeServiceImpl) {
        this.employeeTypeServiceImpl = employeeTypeServiceImpl;
    }

    @Autowired
    public void setEmployeeService(EmployeeService employeeServiceImpl) {
        this.employeeServiceImpl = employeeServiceImpl;
    }
}