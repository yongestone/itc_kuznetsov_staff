package ru.sbrf.itc.staff.actions;

import org.springframework.beans.factory.annotation.Autowired;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.service.Command;
import ru.sbrf.itc.staff.service.EmployeeService;

import java.text.MessageFormat;

public class ShowIdCommand implements Command {
    private EmployeeService employeeServiceImpl;

    public String name() {
        return "showid";
    }

    public String description() {
        return "use \"showid {id}\"";
    }

    public void execute(String[] args) {
        Employee employee = employeeServiceImpl.findEmployee(Long.valueOf(args[args.length - 1]));
        String msg = MessageFormat.format("\tId: {0} , ФИО: {1}, Начальник: {2}, Должность: {3}, Зарплата: {4} , Дата поступления на работу: {5,date,dd-MM-yyyy}\n", employee.getId(), employee.getName(), employee.getParentId(), employee.getEmployeeType().getName(), employee.getSalary(), employee.getReceiptDate());
        System.out.print(msg);
    }

    @Autowired
    public void setEmployeeService(EmployeeService employeeServiceImpl) {
        this.employeeServiceImpl = employeeServiceImpl;
    }
}