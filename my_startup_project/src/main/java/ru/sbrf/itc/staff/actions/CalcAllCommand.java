package ru.sbrf.itc.staff.actions;

import org.springframework.beans.factory.annotation.Autowired;
import ru.sbrf.itc.staff.service.CalcService;
import ru.sbrf.itc.staff.service.Command;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class CalcAllCommand implements Command {
    private CalcService calcServiceImpl;

    public String name() {
        return "calculateall";
    }

    public String description() {
        return "";
    }

    public void execute(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Date in format(YYYY-MM-DD): ");
        Date date = Date.valueOf(in.nextLine());
        BigDecimal finalsalary = calcServiceImpl.calculateAllSalary(date);
        System.out.println(NumberFormat.getNumberInstance(Locale.US).format(finalsalary));
    }

    @Autowired
    public void setCalcService(CalcService calcServiceImpl) {
        this.calcServiceImpl = calcServiceImpl;
    }
}