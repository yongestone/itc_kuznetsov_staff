package ru.sbrf.itc.staff.service;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import org.h2.engine.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.UsesJava7;
import org.springframework.stereotype.Service;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.entity.EmployeeType;
import ru.sbrf.itc.staff.entity.UserRoles;
import ru.sbrf.itc.staff.entity.Users;

import java.io.*;
import java.math.BigDecimal;
import java.sql.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public final class InitHelper {

    private EmployeeTypeService employeeTypeServiceImpl;
    private EmployeeService employeeService;
    private UsersRolesService usersRolesService;
    private UsersService usersService;

    public static final String TYPE_FILE_NAME = "my_startup_project/databaseType.json";
    public static final String EMPLOYEE_FILE_NAME = "my_startup_project/databaseEmployee.json";

    @Autowired
    public void setEmployeeTypeService(EmployeeTypeService employeeTypeService) {
        this.employeeTypeServiceImpl = employeeTypeService;
    }

    @Autowired
    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Autowired
    public void setUsersRolesService(UsersRolesService usersRolesService) {
        this.usersRolesService = usersRolesService;
    }

    @Autowired
    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    public void readType() throws IOException {
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(TYPE_FILE_NAME));
        EmployeeType[] employeeTypeArray = gson.fromJson(reader, EmployeeType[].class);
        for (EmployeeType employeeType : employeeTypeArray) {
            employeeTypeServiceImpl.add(employeeType);
        }
    }

    public void readEmployee() throws IOException, ParseException {
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new FileReader(EMPLOYEE_FILE_NAME));
        Employee[] employeeArray = gson.fromJson(reader, Employee[].class);
        for (Employee employee : employeeArray) {
            employeeService.add(employee);
        }
    }

    public void readRoles() throws IOException, ParseException {
        Users user = new Users(null, "user", "$2a$06$Jn1nYm3SbJ6t4jfQohM6J.2ZTZwPFYzg6ia//2wP4yxB5z.IWpRMq", true);
        usersService.add(user);
        user = new Users(null, "admin", "$2a$06$mPRA/JFgs4HKCkMbhm0b2ukMZ/Lq2Al48jOlW51DZIv8ztG0swI7q", true);
        usersService.add(user);
        UserRoles userRoles = new UserRoles(null, 1L,"user", "ROLE_USER");
        usersRolesService.add(userRoles);
        userRoles = new UserRoles(null, 2L, "admin", "ROLE_USER");
        usersRolesService.add(userRoles);
        userRoles = new UserRoles(null, 2L, "admin", "ROLE_ADMIN");
        usersRolesService.add(userRoles);

    }

    public static void fillTypeToFile() throws Exception {
        Gson gson = new Gson();
        try {
            FileWriter writer = new FileWriter(TYPE_FILE_NAME, false);
            List<EmployeeType> employeeType = new ArrayList<EmployeeType>();
            EmployeeType etype = new EmployeeType(0L, "Employee", BigDecimal.valueOf(0.03), BigDecimal.valueOf(0.3), BigDecimal.valueOf(0), false);
            employeeType.add(etype);
            etype = new EmployeeType(0L, "Manager", BigDecimal.valueOf(0.05), BigDecimal.valueOf(0.4), BigDecimal.valueOf(0.005), true);
            employeeType.add(etype);
            etype = new EmployeeType(0L, "Sales", BigDecimal.valueOf(0.01), BigDecimal.valueOf(0.35), BigDecimal.valueOf(0.003), true);
            employeeType.add(etype);
            gson.toJson(employeeType, writer);
            writer.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void fillEmployeeToFile() throws Exception {
        Gson gson = new Gson();
        try {
            FileWriter writer = new FileWriter(EMPLOYEE_FILE_NAME, false);
            List<Employee> employeeList = new ArrayList<Employee>();
            Employee employee = new Employee(0L, "Ivan Petrov", employeeTypeServiceImpl.findEmployeeType(3L), null, BigDecimal.valueOf(20000), Date.valueOf("2010-10-01"));
            employeeList.add(employee);
            employee = new Employee(0L, "Artur Petrov", employeeTypeServiceImpl.findEmployeeType(2L), 1L, BigDecimal.valueOf(16000), Date.valueOf("2011-10-01"));
            employeeList.add(employee);
            employee = new Employee(0L, "Ilya Harper", employeeTypeServiceImpl.findEmployeeType(1L), 2L, BigDecimal.valueOf(10000), Date.valueOf("2015-11-01"));
            employeeList.add(employee);
            employee = new Employee(0L, "Ilya Fooool", employeeTypeServiceImpl.findEmployeeType(1L), 2L, BigDecimal.valueOf(10000), Date.valueOf("2015-12-01"));
            employeeList.add(employee);
            employee = new Employee(0L, "Engeny Periscop", employeeTypeServiceImpl.findEmployeeType(1L), 2L, BigDecimal.valueOf(9000), Date.valueOf("2015-09-01"));
            employeeList.add(employee);
            employee = new Employee(0L, "Ivan Ivanov", employeeTypeServiceImpl.findEmployeeType(3L), null, BigDecimal.valueOf(21000), Date.valueOf("2009-10-11"));
            employeeList.add(employee);
            employee = new Employee(0L, "Artur Polanski", employeeTypeServiceImpl.findEmployeeType(2L), 6L, BigDecimal.valueOf(15000), Date.valueOf("2012-01-01"));
            employeeList.add(employee);
            employee = new Employee(0L, "Ilya Petro", employeeTypeServiceImpl.findEmployeeType(1L), 7L, BigDecimal.valueOf(10000), Date.valueOf("2014-10-01"));
            employeeList.add(employee);
            employee = new Employee(0L, "Ivan Ivan", employeeTypeServiceImpl.findEmployeeType(2L), 1L, BigDecimal.valueOf(20000), Date.valueOf("2012-11-01"));
            employeeList.add(employee);
            employee = new Employee(0L, "Artur Putin", employeeTypeServiceImpl.findEmployeeType(1L), 9L, BigDecimal.valueOf(10000), Date.valueOf("2016-10-01"));
            employeeList.add(employee);
            employee = new Employee(0L, "Ilya woooooof", employeeTypeServiceImpl.findEmployeeType(1L), 9L, BigDecimal.valueOf(2100), Date.valueOf("2015-10-01"));
            employeeList.add(employee);
            gson.toJson(employeeList, writer);
            writer.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void init() throws Exception {
        readType();
        readEmployee();
        readRoles();
    }
}

