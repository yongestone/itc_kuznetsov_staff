package ru.sbrf.itc.staff.service;

import ru.sbrf.itc.staff.entity.Employee;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Period;
import java.util.List;

public interface CalcService {
    Period getPeriodNow(Employee employee, Date date);

    List<Employee> allSubordinates(Employee employee);

    BigDecimal getMulti(Employee employee, Date date);

    BigDecimal calculateSalesSalary(Employee employee, Date date);

    BigDecimal calculateManagerSalary(Employee employee, Date date);

    BigDecimal calculateSubordinatesSalary(Employee employee, Date date);

    BigDecimal calculateSalary(Employee employee, Date date);

    BigDecimal calculateAllSalary(Date date);
}
