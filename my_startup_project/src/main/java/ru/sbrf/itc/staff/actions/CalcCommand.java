package ru.sbrf.itc.staff.actions;

import org.springframework.beans.factory.annotation.Autowired;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.service.CalcService;
import ru.sbrf.itc.staff.service.Command;
import ru.sbrf.itc.staff.service.EmployeeService;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class CalcCommand implements Command {
    private EmployeeService employeeServiceImpl;
    private CalcService calcServiceImpl;

    public String name() {
        return "calculateid";
    }

    public String description() {
        return "use \"calculateid {id}\"";
    }

    public void execute(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Date in format(YYYY-MM-DD): ");
        Date date = Date.valueOf(in.nextLine());
        Employee employee = employeeServiceImpl.findEmployee(Long.valueOf(args[args.length - 1]));
        BigDecimal finalsalary = calcServiceImpl.calculateSalary(employee, date);
        System.out.println(NumberFormat.getNumberInstance(Locale.US).format(finalsalary));
    }

    @Autowired
    public void setEmployeeService(EmployeeService employeeServiceImpl) {
        this.employeeServiceImpl = employeeServiceImpl;
    }

    @Autowired
    public void setCalcService(CalcService calcServiceImpl) {
        this.calcServiceImpl = calcServiceImpl;
    }
}