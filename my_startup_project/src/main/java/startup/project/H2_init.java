package startup.project;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class H2_init {
    public static void main(String[] args) throws Exception {
        System.out.println("Startup...");

        Class.forName("org.h2.Driver").newInstance();
        Connection connection = DriverManager.getConnection("jdbc:h2:./test", "sa", "");

        Statement statement = null;
        statement = connection.createStatement();
        statement.execute("DROP TABLE IF EXISTS employee");
        statement.execute("CREATE TABLE IF NOT EXISTS employee (id INTEGER PRIMARY KEY AUTO_INCREMENT, name VARCHAR(255), surname VARCHAR(255), age INTEGER)");
        /* */
        statement.execute("INSERT INTO employee (name, surname, age) VALUES('Ivan', 'Ivanov', 1992)");
        statement.execute("INSERT INTO employee (name, surname, age) VALUES('Anton', 'Ivanov', 1985)");
        statement.execute("INSERT INTO employee (name, surname, age) VALUES('Николай', 'Горбачев', 1989)");
        statement.execute("INSERT INTO employee (name, surname, age) VALUES('Владимир', 'Путин', 1992)");
        statement.execute("INSERT INTO employee (name, surname, age) VALUES('Дмитрий', 'Ivanov', 1990)");
        statement.execute("INSERT INTO employee (name, surname, age) VALUES('Дмитрий', 'Медведев', 1996)");
        statement.execute("INSERT INTO employee (name, surname, age) VALUES('Anna', 'Panova', 2001)");
        statement.execute("INSERT INTO employee (name, surname, age) VALUES('Ivan', 'Gorin', 1967)");
        statement.execute("INSERT INTO employee (name, surname, age) VALUES('Ivan', 'Smith', 1992)");
        statement.execute("INSERT INTO employee (name, surname, age) VALUES('Ilya', 'Panin', 1993)");

        System.out.println("CREATE COMPLETE");

        System.out.println("SELECT WHERE name = 'Ivan'");
        ResultSet rs = statement.executeQuery("SELECT * FROM employee WHERE name = 'Ivan'");
        while(rs.next() ) {
            // Retrieve by column name
            int id  = rs.getInt("id") ;
            String name = rs.getString("name") ;
            String surname = rs.getString("surname") ;
            String age = rs.getString("age") ;
            // Display values
            System.out.print("ID: " + id) ;
            System.out.print(", name: " + name) ;
            System.out.print(", surname: " + surname) ;
            System.out.println(", age: " + age) ;
        }

        System.out.println("SELECT WHERE age >= 1992 and <=1993");
        rs = statement.executeQuery("SELECT * FROM employee WHERE age >= 1992 AND age <= 1993");
        while(rs.next() ) {
            // Retrieve by column name
            int id  = rs.getInt("id") ;
            String name = rs.getString("name") ;
            String surname = rs.getString("surname") ;
            String age = rs.getString("age") ;
            // Display values
            System.out.print("ID: " + id) ;
            System.out.print(", name: " + name) ;
            System.out.print(", surname: " + surname) ;
            System.out.println(", age: " + age) ;
        }
        System.out.println("SELECT отсортированный по name");
        rs = statement.executeQuery("SELECT * FROM employee ORDER BY name");
        while(rs.next() ) {
            // Retrieve by column name
            int id  = rs.getInt("id") ;
            String name = rs.getString("name") ;
            String surname = rs.getString("surname") ;
            String age = rs.getString("age") ;
            // Display values
            System.out.print("ID: " + id) ;
            System.out.print(", name: " + name) ;
            System.out.print(", surname: " + surname) ;
            System.out.println(", age: " + age) ;
        }
        System.out.println("Запрос name and age");
        rs = statement.executeQuery("SELECT name,age FROM employee");
        while(rs.next() ) {
            // Retrieve by column name
            String name = rs.getString("name") ;
            String age = rs.getString("age") ;
            // Display values
            System.out.print("name: " + name) ;
            System.out.println(", age: " + age) ;
        }
        statement.close();
        connection.close();
    }
}