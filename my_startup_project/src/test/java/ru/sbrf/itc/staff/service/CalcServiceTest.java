package ru.sbrf.itc.staff.service;

import org.junit.Before;
import org.junit.Test;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.entity.EmployeeType;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CalcServiceTest {
    CalcServiceImpl calcServiceImpl;
    Employee employee1;
    Employee employee2;
    Employee employee3;
    EmployeeType etype1;
    EmployeeType etype2;
    EmployeeType etype3;
    Date date;

    @Before
    public void init() {
        calcServiceImpl = new CalcServiceImpl();
        date = Date.valueOf("2017-06-28");
        etype1 = new EmployeeType(1L, "Employee", BigDecimal.valueOf(0.03), BigDecimal.valueOf(0.3), BigDecimal.valueOf(0), false);
        etype2 = new EmployeeType(2L, "Manager", BigDecimal.valueOf(0.05), BigDecimal.valueOf(0.4), BigDecimal.valueOf(0.005), true);
        etype3 = new EmployeeType(3L, "Sales", BigDecimal.valueOf(0.01), BigDecimal.valueOf(0.35), BigDecimal.valueOf(0.003), true);

        employee3 = new Employee(8L, "Ilya Petro", etype1, 7L, BigDecimal.valueOf(10000), Date.valueOf("2014-10-01"));
        employee2 = new Employee(7L, "Artur Polanski", etype2, 6L, BigDecimal.valueOf(15000), Date.valueOf("2012-01-01"));
        employee1 = new Employee(6L, "Ivan Ivanov", etype3, 0L, BigDecimal.valueOf(21000), Date.valueOf("2009-10-11"));

    }

    @Test
    public void salaryCalculate1() {
        BigDecimal salaryCalculate = calcServiceImpl.calculateSalary(employee3, date);
        //assertTrue(BigDecimal.valueOf(10600).compareTo(salaryCalculate) == 0);
        assertEquals(new BigDecimal("10600").stripTrailingZeros(), salaryCalculate.stripTrailingZeros());
    }

    @Test
    public void salaryCalculate2() {
        BigDecimal salaryCalculate = calcServiceImpl.calculateSalary(employee2, date);

        assertEquals(new BigDecimal("18750").stripTrailingZeros(), salaryCalculate.stripTrailingZeros());
    }

    @Test
    public void salaryCalculate3() {
        BigDecimal salaryCalculate = calcServiceImpl.calculateSalary(employee1, date);

        assertEquals(new BigDecimal("22470").stripTrailingZeros(), salaryCalculate.stripTrailingZeros());
    }

    @Test
    public void salaryCalculate4() {
        List<Employee> employees = new ArrayList<>();
        employees.add(employee3);
        employee2.setSubordinates(employees);
        BigDecimal salaryCalculate = calcServiceImpl.calculateSalary(employee2, date);

        assertEquals(new BigDecimal("18803").stripTrailingZeros(), salaryCalculate.stripTrailingZeros());
    }

    @Test
    public void salaryCalculate5() {
        List<Employee> employees = new ArrayList<>();
        employees.add(employee2);
        employee1.setSubordinates(employees);
        employees = new ArrayList<>();
        employees.add(employee3);
        employee2.setSubordinates(employees);
        BigDecimal salaryCalculate = calcServiceImpl.calculateSalary(employee1, date);

        assertEquals(new BigDecimal("22558.209").stripTrailingZeros(), salaryCalculate.stripTrailingZeros());
    }

}