<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Должность</title>
    <!-- Bootstrap core CSS -->
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/main.css" rel="stylesheet">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../">Главная</a>
        </div>
      </div>
    </div>

    <div class="container">
        <div class="starter-template">
             <h1>Добавление должности</h1>
        </div>
		<form:form action="../registerType" method="post" commandName="typeForm">
          <div class="form-group">
            <label for="NameType">Название должности</label>
            <input type="text" class="form-control" path="name" name="name" id="NameType" placeholder="Enter type">
          </div>
          <div class="form-group">
            <label for="SelectParent">Начальник</label>
            <select class="form-control" name="head" path="head" id="SelectParent">
              <option value=true>Да</option>
              <option value=false>Нет</option>
            </select>
          </div>
          <div class="form-group">
                 <label for="InputRate1">Rate</label>
                 <input type="number" step="0.0001" path="rate" name="rate" class="form-control" id="InputRate1" placeholder="0">
          </div>
          <div class="form-group">
                 <label for="InputRate2">RateMax</label>
                 <input type="number" step="0.0001" path="ratemax" name="ratemax" class="form-control" id="InputRate2" placeholder="0">
          </div>
          <div class="form-group">
                 <label for="InputRate3">RateHead</label>
                 <input type="number" step="0.0001" path="ratehead" name="ratehead" class="form-control" id="InputRate3" placeholder="0">
          </div>
          <button type="submit" class="btn btn-info">Submit</button>
        </form:form>
    </div><!-- /.container -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
  </body>
</html>