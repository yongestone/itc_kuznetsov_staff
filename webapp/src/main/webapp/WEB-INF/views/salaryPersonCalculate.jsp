<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Зарплата сотрудника</title>
    <!-- Bootstrap core CSS -->
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/main.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/resources/css/print.css" media="print" />
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../">Главная</a>
        </div>
      </div>
    </div>

    <div class="container">
		<div class="starter-template">
                <h1>Зарплата сотрудника</h1>
         </div>

		<table class="table table-bordered">
		  <thead>
			<tr>
			  <th>#</th>
			  <th>ФИО</th>
			  <th>Должность</th>
			  <th>Зарплата</th>
			</tr>
		  </thead>
		  <tbody>
                  <tr>
                     <th scope="row">1</th>
                     <td>${person.name}</a></td>
                     <td>${person.employeeType.name}</a></td>
                     <td><fmt:formatNumber value="${salary.stripTrailingZeros()}" minFractionDigits="3"/></td>
                  </tr>
		  </tbody>
		</table>
		<span class="noprint">
            <input type="button" сlasss="btn btn-info" value="Распечатать" onclick="print()"></input>
        </span>
    </div><!-- /.container -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
  </body>
</html>