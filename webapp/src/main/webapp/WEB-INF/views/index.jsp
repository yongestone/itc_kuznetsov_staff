﻿<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Пользователь</title>
<style>
@import url("/resources/css/main.css");
</style>
    <!-- Bootstrap core CSS -->
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name ${person.name}</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
		<div class="author">
			<img class = "author1" src="/resources/img/person.jpg"  alt="author">
			<p></p>
		</div>
		
		<table class="table table-bordered">
		  <thead>
			<tr>
			  <th>#</th>
			  <th>First Name</th>
			  <th>Last Name</th>
			  <th>Username</th>
			</tr>
		  </thead>
		  <tbody>
              <tr>
                  <th scope="row">1</th>
                  <td></td>
                  <td></td>
                  <td></td>
              </tr>
		  </tbody>
		</table>
    </div><!-- /.container -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
  </body>
</html>