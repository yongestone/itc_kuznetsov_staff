<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include.jsp" %>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Главная</title>
    <!-- Bootstrap core CSS -->
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="<c:url value="/resources/css/main.css"/>" rel="stylesheet">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Главная</a>
        </div>
        <div class="collapse navbar-collapse">
           <ul class="nav navbar-nav">
             <li><a href="/calcall">Расчитать общую зарплату</a></li>
             <li><a href="<c:url value="/logout"/>" > Logout</a></li>
           </ul>
        </div>
      </div>
    </div>

    <div class="container">
        <c:if test="${not empty msg}">
        	<div class="alert alert-${css} alert-dismissible" role="alert">
        		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        				<span aria-hidden="true">×</span>
        		</button>
        		<strong>${msg}</strong>
        	</div>
        </c:if>
        <div class="starter-template">
              <h1>Главная страница</h1>
        </div>
        <div class="row">
          <div class="col-sm-7">
          <div class="starter-template">
             <h2>Сотрудники</h2>
          </div>
          <table class="table table-bordered">
          		  <thead>
          			<tr>
          			  <th>#</th>
          			  <th>ФИО</th>
          			  <th>Должность</th>
          			  <th></th>
          			  <th></th>
          			  <th></th>
          			</tr>
          		  </thead>
          		  <tbody>
          		    <c:forEach items="${employeeList}" var="item" varStatus="count">
                        <tr>
                            <th scope="row">${count.count}</th>
                            <td><a href = "/person/${item.id}">${item.name}</a></td>
                            <td>${item.employeeType.name}</td>
                            <td><button onclick="location.href='/changePerson/${item.id}'">Изменить</button></td>
                            <td><button onclick="location.href='/removeuser/${item.id}'">Удалить</button></td>
                            <td><button onclick="location.href='/calc/${item.id}'">Расчитать зарплату</button></td>
                        </tr>
                    </c:forEach>
          		  </tbody>
          </table>
          <button class="btn btn-info" onclick="location.href='/person/add'">Добавить пользователя</button>
          </div>


          <div class="col-sm-5">
          <div class="starter-template">
               <h2>Должности</h2>
          </div>
          <table class="table table-bordered">
          		  <thead>
          			<tr>
          			  <th>#</th>
          			  <th>Название должности</th>
          			  <th></th>
          			  <th></th>
          			</tr>
          		  </thead>
          		  <tbody>
          		    <c:forEach items="${employeeTypeList}" var="item" varStatus="count">
                        <tr>
                            <th scope="row">${count.count}</th>
                            <td><a href = "/type/${item.id}">${item.name}</a></td>
                            <td><button onclick="location.href='/changeType/${item.id}'">Изменить</button></td>
                            <td><button onclick="location.href='/removetype/${item.id}'">Удалить</button></td>
                        </tr>
                    </c:forEach>
          		  </tbody>
          </table>
          <button class="btn btn-info" onclick="location.href='/type/add'">Добавить должность</button>
          </div>
        </div>
    </div><!-- /.container -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
  </body>
</html>