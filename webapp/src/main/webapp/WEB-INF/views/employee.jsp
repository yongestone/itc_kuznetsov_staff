<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Пользователь</title>
    <!-- Bootstrap core CSS -->
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/main.css" rel="stylesheet">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../">Главная</a>
        </div>
      </div>
    </div>

    <div class="container">
		<div class="author">
			<img class = "author1" src="/resources/img/person.jpg"  alt="author">
			<p>Name : ${person.name}</p>
			<p>EmplType : <a href = "../type/${person.employeeType.id}">${person.employeeType.name}</a></p>
			<p>Date : ${person.receiptDate}</p>
			<p>BaseSalary : ${person.salary}</p>
			<c:if test = "${not empty parent}">
                 <p>Непосредственный начальник : <a href = "${parent.id}">${parent.name}</a></p>
            </c:if>
            <button class = "btn btn-basic" onclick="location.href='../calc/${person.id}'">Расчитать зарплату</button>
		</div>

		<c:if test = "${not empty allSubordinate}">
		    <div class="starter-template">
                <h1>Подчиненные</h1>
            </div>

		<table class="table table-bordered">
		  <thead>
			<tr>
			  <th>#</th>
			  <th>ФИО</th>
			</tr>
		  </thead>
		  <tbody>
		       <c:forEach items="${person.subordinates}" var="item" varStatus="count">
                    <tr>
                        <th scope="row">${count.count}</th>
                        <td><a href = "${item.id}">${item.name}</a></td>
                    </tr>
               </c:forEach>
		  </tbody>
		</table>

		</c:if>
    </div><!-- /.container -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
  </body>
</html>