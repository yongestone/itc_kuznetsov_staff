<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Должность</title>
    <!-- Bootstrap core CSS -->
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/main.css" rel="stylesheet">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../">Главная</a>
        </div>
      </div>
    </div>

    <div class="container">
		<div class="author">
			<p>Name : ${personType.name}</p>
			<p>Rate :  ${personType.rate}</p>
			<p>RateHead : ${personType.ratehead}</p>
			<p>RateMax :  ${personType.ratemax}</p>
		</div>
		<div class="starter-template">
                <h1>Сотрудники</h1>
         </div>

		<table class="table table-bordered">
		  <thead>
			<tr>
			  <th>#</th>
			  <th>ФИО</th>
			</tr>
		  </thead>
		  <tbody>
              <c:forEach items="${personType.employees}" var="item" varStatus="count">
                  <tr>
                      <th scope="row">${count.count}</th>
                      <td><a href = "../person/${item.id}"> ${item.name}</a></td>
                  </tr>
              </c:forEach>
		  </tbody>
		</table>

    </div><!-- /.container -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
  </body>
</html>