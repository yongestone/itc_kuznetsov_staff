<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Пользователь</title>
    <!-- Bootstrap core CSS -->
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/css/main.css" rel="stylesheet">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../">Главная</a>
        </div>
      </div>
    </div>

    <div class="container">
        <div class="starter-template">
             <h1>Добавление сотрудника</h1>
        </div>
		 <form:form action="../registerUser" method="post" commandName="userForm">
          <div class="form-group">
            <label for="exampleInputName1">ФИО сотрудника</label>
            <input type="text" class="form-control" path="name" name="name" placeholder="Enter name">
          </div>
          <div class="form-group">
            <label for="exampleSelect1">Должность</label>
            <select class="form-control" name="type" id="type">
              <c:forEach items = "${employeeTypeList}" var = "item">
                    <option value = "${item.id}">${item.name}</option>
              </c:forEach>
            </select>
          </div>
           <div class="form-group">
              <label for="exampleSelect2">Начальник</label>
              <select class="form-control" name="parent" id="exampleSelect2">
                <option value="">Нет начальника</option>
                <c:forEach items = "${parentList}" var = "item">
                     <option value = ${item.id}>${item.name} (${item.employeeType.name})</option>
                </c:forEach>
              </select>
           </div>
            <div class="form-group">
                 <label for="exampleInputSalary1">Ставка сотрудника</label>
                 <input type="number" step="0.01" name="salary" path="salary" class="form-control" id="exampleInputSalary1" placeholder="0">
             </div>
              <div class="form-group">
                <label for="inputDate">Введите дату поступления:</label>
                <input type="date" name="date" class="form-control">
              </div>
          <button type="submit" class="btn btn-info">Submit</button>
        </form:form>
    </div><!-- /.container -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
  </body>
</html>