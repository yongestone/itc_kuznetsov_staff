<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page pageEncoding="UTF-8" %>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>Login Page</title>
  <link href="/resources/css/bootstrap.min.css" rel="stylesheet"/>
  <link href="/resources/css/main.css" rel="stylesheet">
</head>
<body>
<h1>Login page</h1>
<p>Valid users:
<p>username: <b>user</b>, password: <b>user</b></p>
<p>username: <b>admin</b>, password: <b>admin</b></p>

<div class="container">
      <form name="frm" class="form-signin" action="login" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="text" class="form-control" placeholder="username" name="username" required autofocus>
        <input type="password" class="form-control" placeholder="Password" name="password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <input type="hidden" name="<c:out value="${_csrf.parameterName}"/>"
                 value="<c:out value="${_csrf.token}"/>"/>
      </form>

        <c:if test="${not empty param.login_error}">
          <span style="color: red; ">
            Your login attempt was not successful, try again.<br/><br/>
            Reason: <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>
          </span>
        </c:if>
</div>
</body>

</html>
