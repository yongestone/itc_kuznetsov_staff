package ru.sbrf.itc.staff.web;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import ru.sbrf.itc.staff.entity.UserRoles;
import ru.sbrf.itc.staff.entity.Users;
import ru.sbrf.itc.staff.service.UsersRolesService;
import ru.sbrf.itc.staff.service.UsersService;

public class MyUserDetailsService implements UserDetailsService {

    private UsersService usersService;
    private UsersRolesService usersRolesService;

    @Autowired
    public MyUserDetailsService(UsersService usersService, UsersRolesService usersRolesService) {
        this.usersService = usersService;
        this.usersRolesService = usersRolesService;
    }

    @Override
    public UserDetails loadUserByUsername(final String username)
            throws UsernameNotFoundException {

        Users user = usersService.findEmployee(username);
        Set<UserRoles> list  = new HashSet<UserRoles>(usersRolesService.findAll(username));
        List<GrantedAuthority> authorities = buildUserAuthority(list);

        return buildUserForAuthentication(user, authorities);
    }

    // Converts com.mkyong.users.model.User user to
    // org.springframework.security.core.userdetails.User
    private User buildUserForAuthentication(Users user,
                                            List<GrantedAuthority> authorities) {
        return new User(user.getName(),
                user.getPassword(), user.isEnabled(),
                true, true, true, authorities);
    }

    private List<GrantedAuthority> buildUserAuthority(Set<UserRoles> userRoles) {

        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

        // Build user's authorities
        for (UserRoles userRole : userRoles) {
            setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
        }

        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

        return Result;
    }
}

