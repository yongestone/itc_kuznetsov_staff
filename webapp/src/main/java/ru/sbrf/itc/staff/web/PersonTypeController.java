package ru.sbrf.itc.staff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sbrf.itc.staff.entity.EmployeeType;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

@Controller
public class PersonTypeController {
    @Autowired
    private EmployeeTypeService employeeServiceType;

    @RequestMapping("/type/{typeId}")
    public String show(@PathVariable String typeId, ModelMap map){
        EmployeeType personType = employeeServiceType.findEmployeeType(Long.valueOf(typeId));
        map.addAttribute("personType", personType);
        return "employeeType";
    }
}
