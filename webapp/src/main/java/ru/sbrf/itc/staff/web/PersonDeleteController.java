package ru.sbrf.itc.staff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.sbrf.itc.staff.service.EmployeeService;

@Controller
public class PersonDeleteController {
    @Autowired
    EmployeeService employeeService;

    @RequestMapping("/removeuser/{id}")
    public String removeUser(@PathVariable Long id, final RedirectAttributes redirectAttributes){
        employeeService.delete(employeeService.findEmployee(id));
        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Пользователь успешно удален");
        return "redirect:/admin";
    }
}
