package ru.sbrf.itc.staff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.sbrf.itc.staff.entity.EmployeeType;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

@Controller
public class TypeDeleteController {
    @Autowired
    EmployeeTypeService employeeTypeService;

    @RequestMapping("/removetype/{id}")
    public String removeType(@PathVariable Long id, final RedirectAttributes redirectAttributes){
        EmployeeType employeeType = employeeTypeService.findEmployeeType(id);
        if(employeeType.getEmployees().size() == 0){
            employeeTypeService.delete(employeeType);
            redirectAttributes.addFlashAttribute("css", "success");
            redirectAttributes.addFlashAttribute("msg", "Должность успешно удалена");
        }else{
            redirectAttributes.addFlashAttribute("css", "danger");
            redirectAttributes.addFlashAttribute("msg", "Нельзя удалить должность. Измените или удалите пользователей с этой должностью.");
        }
        return "redirect:/admin";
    }
}
