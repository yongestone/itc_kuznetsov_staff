package ru.sbrf.itc.staff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.service.CalcService;
import ru.sbrf.itc.staff.service.EmployeeService;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class CalcController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeTypeService employeeTypeService;
    @Autowired
    private CalcService calcService;

    @RequestMapping("/calcall")
    public String calculateall(ModelMap map){
        Date date = Date.valueOf(LocalDate.now());
        BigDecimal result = BigDecimal.ZERO;

        Map<Employee, BigDecimal> counts = new HashMap<>();
        List<Employee> employeeList = employeeService.findAll();
        for (Employee employee : employeeList) {
            BigDecimal salary = calcService.calculateSalary(employee, date);
            counts.put(employee, salary);
            result = result.add(salary);
        }

        map.addAttribute("resultSalary", result);
        map.addAttribute("salary", counts);
        return "salaryAllCalculate";
    }

}

