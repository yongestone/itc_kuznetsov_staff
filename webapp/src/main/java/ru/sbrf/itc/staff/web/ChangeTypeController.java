package ru.sbrf.itc.staff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.sbrf.itc.staff.entity.EmployeeType;
import ru.sbrf.itc.staff.service.EmployeeService;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

import java.lang.reflect.InvocationTargetException;

@Controller
public class ChangeTypeController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeTypeService employeeTypeService;

    @RequestMapping("/changeType/{typeId}")
    public String typeChange(@PathVariable String typeId, ModelMap model){
        EmployeeType employeeType = employeeTypeService.findEmployeeType(Long.valueOf(typeId));
        model.addAttribute("type", employeeType);
        return "changeType";
    }


    @RequestMapping(value = "/updateType", method = RequestMethod.POST)
    public String updateType(@ModelAttribute("typeForm") EmployeeType employeeType, final RedirectAttributes redirectAttributes) throws InvocationTargetException, IllegalAccessException {
        employeeTypeService.update(employeeType);
        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Информация должности изменена");
        return "redirect:/admin";
    }
}
