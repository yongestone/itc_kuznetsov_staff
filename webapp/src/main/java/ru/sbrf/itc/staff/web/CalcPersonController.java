package ru.sbrf.itc.staff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.service.CalcService;
import ru.sbrf.itc.staff.service.EmployeeService;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class CalcPersonController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeTypeService employeeTypeService;
    @Autowired
    private CalcService calcService;

    @RequestMapping("/calc/{personId}")
    public String calculate(@PathVariable String personId, ModelMap map){
        Date date = Date.valueOf(LocalDate.now());
        Employee employee = employeeService.findEmployee(Long.valueOf(personId));
        BigDecimal result = calcService.calculateSalary(employee, date);
        map.addAttribute("salary", result);
        map.addAttribute("person", employee);
        return "salaryPersonCalculate";
    }

}

