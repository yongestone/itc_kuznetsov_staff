package ru.sbrf.itc.staff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.entity.EmployeeType;
import ru.sbrf.itc.staff.service.EmployeeService;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

import java.sql.Date;
import java.util.List;

@Controller
public class TypeAddController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeTypeService employeeTypeService;

    @RequestMapping("/type/add")
    public String typeAdd(){
        return "formType";
    }

    @RequestMapping(value = "/registerType", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("typeForm") EmployeeType employeeType, final RedirectAttributes redirectAttributes) {
        employeeTypeService.add(employeeType);
        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Должность успешно создана");
        return "redirect:/admin";
    }
}
