package ru.sbrf.itc.staff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.entity.EmployeeType;
import ru.sbrf.itc.staff.service.EmployeeService;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

import java.sql.Date;
import java.util.List;

@Controller
public class PersonAddController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeTypeService employeeTypeService;

    @RequestMapping("/person/add")
    public String personAdd(ModelMap map){
        List<Employee> parentList = employeeService.findParent();
        List<EmployeeType> employeeTypeList = employeeTypeService.findAll();
        map.addAttribute("parentList", parentList);
        map.addAttribute("employeeTypeList", employeeTypeList);
        return "formUser";
    }

    @RequestMapping(value = "/registerUser", method = RequestMethod.POST)
    public String addUser(final RedirectAttributes redirectAttributes, @ModelAttribute("userForm") Employee employee,
                          @RequestParam("parent") Long parentId, @RequestParam("type") Long typeId, @RequestParam("date") Date date) {
        employee.setParentId(parentId);
        employee.setReceiptDate(date);
        employee.setEmployeeType(employeeTypeService.findEmployeeType(typeId));
        employeeService.add(employee);
        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Пользователь успешно создан");
        return "redirect:/admin";
    }
}
