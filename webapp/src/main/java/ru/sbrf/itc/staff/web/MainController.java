package ru.sbrf.itc.staff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.entity.EmployeeType;
import ru.sbrf.itc.staff.service.EmployeeService;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

import java.util.List;

@Controller
public class MainController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeTypeService employeeTypeService;

    @RequestMapping("/")
    public String show(ModelMap map) {
        return "redirectPage";
    }

    @RequestMapping("/admin")
    public String admin(ModelMap map) {
        List<Employee> employeeList = employeeService.findAll();
        List<EmployeeType> employeeTypeList = employeeTypeService.findAll();
        map.addAttribute("employeeList", employeeList);
        map.addAttribute("employeeTypeList", employeeTypeList);
        return "main";
    }

}

