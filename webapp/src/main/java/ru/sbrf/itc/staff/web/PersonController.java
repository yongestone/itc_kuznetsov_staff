package ru.sbrf.itc.staff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.service.CalcService;
import ru.sbrf.itc.staff.service.EmployeeService;

@Controller
public class PersonController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private CalcService calcService;

    @RequestMapping("/person/{personId}")
    public String show(@PathVariable String personId, ModelMap map){
        Employee person = employeeService.findEmployee(Long.valueOf(personId));
        Employee parent = null;
        if(person.getParentId() != null){
            parent = employeeService.findEmployee(person.getParentId());
        }
        map.addAttribute("person", person);
        map.addAttribute("allSubordinate", calcService.allSubordinates(person));
        map.addAttribute("parent", parent);
        return "employee";
    }
}
