package ru.sbrf.itc.staff.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.sbrf.itc.staff.entity.Employee;
import ru.sbrf.itc.staff.entity.EmployeeType;
import ru.sbrf.itc.staff.service.EmployeeService;
import ru.sbrf.itc.staff.service.EmployeeTypeService;

import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.util.List;

@Controller
public class ChangePersonController {
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeTypeService employeeTypeService;

    @RequestMapping("/changePerson/{personId}")
    public String changePerson(@PathVariable String personId, ModelMap map){
        Employee person = employeeService.findEmployee(Long.valueOf(personId));
        List<Employee> parentList = employeeService.findParent();
        List<EmployeeType> employeeTypeList = employeeTypeService.findAll();
        map.addAttribute("parentList", parentList);
        map.addAttribute("employeeTypeList", employeeTypeList);
        map.addAttribute("employeeClass", person);
        return "changeUser";
    }

    @RequestMapping(value = "/changeUser", method = RequestMethod.POST)
    public String updateUser(final RedirectAttributes redirectAttributes, @ModelAttribute("changeUser") Employee employee,
                             @RequestParam("parent") Long parentId, @RequestParam("type") Long typeId, @RequestParam("date") Date date) throws InvocationTargetException, IllegalAccessException {
        employee.setParentId(parentId);
        employee.setReceiptDate(date);
        employee.setEmployeeType(employeeTypeService.findEmployeeType(typeId));
        employeeService.update(employee);
        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Информация пользователя изменена");
        return "redirect:/admin";
    }
}
